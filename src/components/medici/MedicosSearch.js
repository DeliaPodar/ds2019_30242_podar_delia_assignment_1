
export const colunas = {
  NOME_MEDICO: 'nome_medico',
  ESPECIALIDADE: 'especialidades.nome_especialidade'
}

const ROWS_PER_PAGE = 10

class MedicosSearch {

  constructor() {
    this.initialTableState()
    this.initialPaginationState()
  }

  copy() {
    const medicosSearch = new MedicosSearch()

    medicosSearch.nomeMedico = this.nomeMedico
    medicosSearch.cpf = this.cpf
    medicosSearch.estadoCrm = this.estadoCrm
    medicosSearch.especialidade = this.especialidade

    medicosSearch.sortColumn = this.sortColumn
    medicosSearch.sortDirection = this.sortDirection

    medicosSearch.currentPage = this.currentPage
    
    return medicosSearch
  }

  initialTableState() {
    this.nomeMedico = ''
    this.cpf = ''
    this.estadoCrm = ''
    this.especialidade = ''

    this.sortColumn = colunas.NOME_MEDICO
    this.sortDirection = 'asc'
  }

  initialPaginationState() {
    this.rowsPerPage = ROWS_PER_PAGE
    this.currentPage = 0
  }

  reiniciarAtributos() {
    this.initialTableState()
    this.currentPage = 0
  }

  getNumberOfPagesForPagination(rows) {
    let numPages = 0
    if(rows % this.rowsPerPage === 0) {
      numPages = Math.floor(rows / this.rowsPerPage)
    } else {
      numPages = Math.floor(rows / this.rowsPerPage) + 1
    }
    
    return numPages
  }

  changeSort(column) {
    if(this.sortColumn === column) {
      if(this.sortDirection === 'asc') {
        this.sortDirection = 'desc';
      } else {
        this.sortDirection = 'asc';
      }
    }

    this.sortColumn = column;
  }

}

export default MedicosSearch