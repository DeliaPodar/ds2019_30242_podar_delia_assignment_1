import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Button, Icon, Modal, Form, Message } from 'semantic-ui-react'

import EspecialidadeAPI from '../../api/EspecialidadeAPI'

class EspecialidadeModal extends Component {

  static propTypes = {
    icon: PropTypes.string
  }

  constructor(props){
    super(props)

    this.state = {
      nome_especialidade: '',
      modalOpen: false,
      formClassName: '',
      formSuccessMessage: '',
      formErrorMessage: ''
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setInitialStateModal = () => {
    this.setState({
      nome_especialidade: '',
      modalOpen: false,
      formClassName: '',
      formSuccessMessage: '',
      formErrorMessage: ''
    });
  }

  handleOpen = (e) => {
    this.setState({ modalOpen: true })

    // verifico se eh um cadastro de especialidade
    if(!this.props.idEspecialidade) {
      this.setState({
        nome_especialidade: '',
        formClassName: '',
        formSuccessMessage: '',
        formErrorMessage: ''
      })
    }
  }

  handleClose = (e) => this.setInitialStateModal()
  
  handleBeforeOpen = (e) => {
    if (this.props.idEspecialidade) {
      // edicao de uma especialidade
      EspecialidadeAPI.getById(this.props.idEspecialidade)
        .done((response) => {
          this.setState({
            nome_especialidade: (response.nome_especialidade === null) ? '' : response.nome_especialidade
          })
        })
        .fail(() => {})
    }
  }

  handleInputChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({ [name]: value });
  }

  atualizar(especialidade) {
    EspecialidadeAPI.put(especialidade)
      .done((response) => {
        this.setState({
          modalOpen: false
        });

        this.props.atualizarEspecialidades();
      })
      .fail(() => {
        this.setState({
          formClassName: 'warning',
          formErrorMessage: 'erro ao atualizar...'
        });
      });
  }

  salvar(especialidade) {
    EspecialidadeAPI.post(especialidade)
      .done((response) => {
        this.setState({
          modalOpen: false
        });

        this.props.atualizarEspecialidades();
      })
      .fail(() => {
        this.setState({
          formClassName: 'warning',
          formErrorMessage: 'erro ao adicionar...'
        });
      });
  }

  handleSubmit(e) {
    // Prevent browser refresh
    e.preventDefault();

    //console.log(this.state.nome_especialidade);

    let especialidade = {
      nome_especialidade: this.state.nome_especialidade
    };

    if(this.props.idEspecialidade) {
      especialidade = {
        id: this.props.idEspecialidade,
        ...especialidade
      };

      this.atualizar(especialidade);
    } else {
      this.salvar(especialidade);
    }
  }

  createButtonTrigger = () => {
    if(this.props.icon) {
      return (
        <Button icon color={this.props.buttonColor} onClick={this.handleOpen}>
          <Icon name={this.props.icon} />
        </Button>
      )
    }
    return <Button color={this.props.buttonColor} onClick={this.handleOpen}>{this.props.buttonTriggerTitle}</Button>
  }

  render() {
    const formClassName = this.state.formClassName;
    const formSuccessMessage = this.state.formSuccessMessage;
    const formErrorMessage = this.state.formErrorMessage;

    return (
      <Modal
        trigger={this.createButtonTrigger()}
        dimmer={true}
        size='small'
        closeIcon='close'
        open={this.state.modalOpen}
        onOpen={() => this.handleBeforeOpen()}
        onClose={() => this.handleClose()}>
        <Modal.Header>{this.props.headerTitle}</Modal.Header>
        <Modal.Content>
          {this.props.idEspecialidade && <span>ID da Especialidade : {this.props.idEspecialidade}</span>}

          <Form className={formClassName} onSubmit={this.handleSubmit}>
            <Form.Input
              label='Nome'
              type='text'
              placeholder='Nome'
              name='nome_especialidade'
              maxLength='255'
              required
              value={this.state.nome_especialidade}
              onChange={this.handleInputChange} />

            <Message
              success
              color='green'
              header='Nice one!'
              content={formSuccessMessage} />
            <Message
              warning
              color='yellow'
              header='Woah!'
              content={formErrorMessage} />

            <div style={{float: 'left'}}>
              <Button className='ui-modal-button' color='red' onClick={this.handleClose}>Cancelar</Button>
              <Button className='ui-modal-button' color={this.props.buttonColor}>{this.props.buttonSubmitTitle}</Button>
            </div>
            <br />
          </Form>
        </Modal.Content>
      </Modal>
    );
  }

}

export default EspecialidadeModal;