
export const colunas = {
  NOME_ESPECIALIDADE: 'nome_especialidade',
  CREATED_AT: 'created_at',
  UPDATED_AT: 'updated_at'
}

class EspecialidadesSearch {

  constructor() {
    this.initialTableState()

    this.paginationState = {
      rowsPerPage: 10,
      currentPage: 0
    }
  }

  changeSort(column) {
    if(this.tableState.sortColumn === column) {
      if(this.tableState.sortDirection === 'asc') {
        this.tableState.sortDirection = 'desc';
      } else {
        this.tableState.sortDirection = 'asc';
      }
    }

    this.tableState.sortColumn = column;
  }

  getNumberOfPagesForPagination(rows) {
    let numPages = 0
    if(rows % this.paginationState.rowsPerPage === 0) {
      numPages = Math.floor(rows / this.paginationState.rowsPerPage)
    } else {
      numPages = Math.floor(rows / this.paginationState.rowsPerPage) + 1
    }
    
    return numPages
  }

  reiniciarAtributos() {
    this.initialTableState()

    this.paginationState.currentPage = 0
  }

  initialTableState() {
    this.tableState = {
      nomeEspecialidade: '',
      dataCadastroInicial: '',
      dataCadastroFinal: '',
      sortColumn: colunas.NOME_ESPECIALIDADE,
      sortDirection: 'asc'
    }
  }

}

export default EspecialidadesSearch;