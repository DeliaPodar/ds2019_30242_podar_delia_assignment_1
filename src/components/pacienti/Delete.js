import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { Button, Modal, Icon } from 'semantic-ui-react';

class EspecialidadeModalDelete extends Component {

  static propTypes = {
    icon: PropTypes.string
  }

  constructor(props) {
    super(props);

    this.state = {
      modalOpen: false
    };
  }

  handleOpen = e => this.setState({ modalOpen: true })

  handleClose = e => this.setState({ modalOpen: false })

  handleSubmit = (e) => {
    let especialidadeID = e.target.getAttribute('data-especialidadeID');
    this.props.onEspecialidadeDeleted(especialidadeID);
    this.handleClose();
  }

  createButtonTrigger = () => {
    if(this.props.icon) {
      return (
        <Button icon color={this.props.buttonColor} onClick={this.handleOpen}>
          <Icon name={this.props.icon} />
        </Button>
      )
    }
    return <Button color={this.props.buttonColor} onClick={this.handleOpen}>{this.props.buttonTriggerTitle}</Button>
  }

  render() {
    return (
      <Modal
        trigger={this.createButtonTrigger()}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        dimmer='inverted'
        size='small'>
        <Modal.Header>{this.props.headerTitle}</Modal.Header>
        <Modal.Content>
          <p>Você tem certeza que deseja remover a Especialidade de nome: <strong>{this.props.especialidade.nome_especialidade}</strong>?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.handleSubmit} data-especialidadeID={this.props.especialidade.id} color='red'>Sim</Button>
          <Button onClick={this.handleClose} color='black'>Não</Button>
        </Modal.Actions>
      </Modal>
    );
  }

}

export default EspecialidadeModalDelete;