
import $ from 'jquery'

import API, { BASE_URL } from './API'

class MedicoAPI extends API {

  static getAll() {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos`, 
      method: 'GET' 
    })

    return request
  }

  static searchAll(medicosSearch, searchParams) {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos/search`, 
      method: 'POST',
      data: {
        nome_medico: searchParams.nomeMedico,
        cpf: searchParams.cpf,
        estado_crm_id: searchParams.estadoCrm,
        especialidade_id: searchParams.especialidade,

        sort_column: medicosSearch.sortColumn,
        sort_direction: medicosSearch.sortDirection,
        rows_per_page: medicosSearch.rowsPerPage,
        current_page: medicosSearch.currentPage
      }
    })

    return request
  }

  static getById(id) {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos/${id}`, 
      method: 'GET' 
    })

    return request
  }

  static post(medico) {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos`, 
      method: 'POST',
      data: medico
    })
    
    return request
  }

  static put(medico) {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos/${medico.id}`, 
      method: 'PUT',
      data: medico
    })

    return request
  }

  static delete(idMedico) {
    let request = $.ajax({ 
      url: `${BASE_URL}/medicos/${idMedico}`, 
      method: 'DELETE'
    })

    return request
  }

}

export default MedicoAPI