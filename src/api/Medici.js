import $ from 'jquery';

class EspecialidadeAPI {

  static getAll() {
    let request = $.ajax({ 
      url: 'http://localhost:3000/especialidades', 
      method: 'GET' 
    });
    return request;
  }

  static searchAll(especialidadesSearch) {
    let dataCadastroInicial = '';
    if(especialidadesSearch.tableState.dataCadastroInicial) {
      dataCadastroInicial = especialidadesSearch.tableState.dataCadastroInicial + " 00:00:00";
    }

    let dataCadastroFinal = '';
    if(especialidadesSearch.tableState.dataCadastroFinal) {
      dataCadastroFinal = especialidadesSearch.tableState.dataCadastroFinal + " 23:59:59";
    }
    
    let request = $.ajax({ 
      url: 'http://localhost:3000/especialidades/search', 
      method: 'POST',
      data: {
        nome_especialidade: especialidadesSearch.tableState.nomeEspecialidade,
        created_at_inicio: dataCadastroInicial,
        created_at_final: dataCadastroFinal,
        sort_column: especialidadesSearch.tableState.sortColumn,
        sort_direction: especialidadesSearch.tableState.sortDirection,
        rows_per_page: especialidadesSearch.paginationState.rowsPerPage,
        current_page: especialidadesSearch.paginationState.currentPage
      } 
    });
    return request;
  }

  static getCount() {
    let request = $.ajax({ 
      url: 'http://localhost:3000/especialidade/count', 
      method: 'GET' 
    });
    return request;
  }

  static getById(id) {
    let request = $.ajax({ 
      url: `http://localhost:3000/especialidades/${id}`, 
      method: 'GET' 
    });
    return request;
  }

  static post(especialidade) {
    let request = $.ajax({ 
      url: 'http://localhost:3000/especialidades', 
      method: 'POST',
      data: especialidade
    });
    return request;
  }

  static put(especialidade) {
    let request = $.ajax({ 
      url: `http://localhost:3000/especialidades/${especialidade.id}`, 
      method: 'PUT',
      data: especialidade
    });
    return request;
  }

  static delete(idEspecialidade) {
    let request = $.ajax({ 
      url: `http://localhost:3000/especialidades/${idEspecialidade}`, 
      method: 'DELETE'
    });
    return request;
  }

}

export default EspecialidadeAPI;