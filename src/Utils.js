
class Utils {

  static listMethodsFromObject(obj) {
    var methods = [];
    for (var m in obj) {        
      if (typeof obj[m] === "function" && obj.hasOwnProperty(m)) {
        methods.push(m);
      }
    }
    console.log(methods.join(","));
  }

}